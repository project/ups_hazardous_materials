<?php

declare(strict_types=1);

namespace Drupal\ups_hazardous_materials\EventSubscriber;

use Drupal\commerce_checkout\CheckoutOrderManagerInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowWithPanesInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Url;
use Drupal\ups_hazardous_materials\UpsHazardousManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Drupal\ups_hazardous_materials\FreightManager;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * UPS Hazardous Materials event subscriber.
 */
class UpsHazardousMaterialsSubscriber implements EventSubscriberInterface {
  use StringTranslationTrait;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The freight shipping manager.
   *
   * @var \Drupal\ups_hazardous_materials\FreightManager
   */
  protected $freightManager;

  /**
   * Current route match service.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $routeMatch;

  /**
   * The UPS Hazardous manager.
   *
   * @var \Drupal\ups_hazardous_materials\UpsHazardousManager
   */
  protected $upsHazardousManager;

  /**
   * The checkout order manager.
   *
   * @var \Drupal\commerce_checkout\CheckoutOrderManagerInterface
   */
  protected $checkoutOrderManager;

  /**
   * Constructs event subscriber.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\ups_hazardous_materials\FreightManager $freight_manager
   *   The fright shipping manager.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $route_match
   *   The current route match service.
   * @param \Drupal\ups_hazardous_materials\UpsHazardousManager $ups_hazardous_manager
   *   The UPS Hazardous manager.
   * @param \Drupal\commerce_checkout\CheckoutOrderManagerInterface $checkout_order_manager
   *   The checkout order manager.
   */
  public function __construct(MessengerInterface $messenger, FreightManager $freight_manager, CurrentRouteMatch $route_match, UpsHazardousManager $ups_hazardous_manager, CheckoutOrderManagerInterface $checkout_order_manager) {
    $this->messenger = $messenger;
    $this->freightManager = $freight_manager;
    $this->routeMatch = $route_match;
    $this->upsHazardousManager = $ups_hazardous_manager;
    $this->checkoutOrderManager = $checkout_order_manager;
  }

  /**
   * Kernel request event handler.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   *   Response event.
   */
  public function onKernelRequest(GetResponseEvent $event): void {
    // All the verifications regard the checkout process.
    if ($this->routeMatch->getRouteName() != 'commerce_checkout.form') {
      return;
    }
    $order = $event->getRequest()->attributes->get('commerce_order');

    // Let's check for freight items in the current user cart.
    if (!$this->freightManager->canCheckout($order)) {
      // Get back to cart page if any freight shipping element is found.
      $this->messenger->addWarning($this->t('You cannot proceed to checkout with "Freight Shipping" items in your cart. Please remove them to continue.'));
      $event->setResponse(new RedirectResponse(Url::fromRoute('commerce_cart.page')->toString(), 303));
    }

    // If we're viewing the review checkout page, check for hazardous items.
    if ($this->routeMatch->getParameter('step') == 'review') {
      $checkout_flow = $this->checkoutOrderManager->getCheckoutFlow($order);
      $configuration = [];

      // We must retrieve the hazardous configuration from the pane configs.
      if ($checkout_flow->getPlugin() instanceof CheckoutFlowWithPanesInterface) {
        $pane = $checkout_flow->getPlugin()->getPane('ups_hazardous');
        $configuration = $pane->getConfiguration();
      }

      // If the order isn't valid then redirect the user to the order info step.
      if (!$this->upsHazardousManager->validateOrder($order, $configuration)) {
        $this->messenger->addError($this->t("The hazardous item shipment has not been validated. Please contact the administrator."));
        $event->setResponse(new RedirectResponse(Url::fromRoute(
          'commerce_checkout.form',
          [
            'commerce_order' => $order->id(),
            'step' => 'order_information',
          ])->toString(), 303));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      KernelEvents::REQUEST => ['onKernelRequest'],
    ];
  }

}

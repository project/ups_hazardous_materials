<?php

declare(strict_types=1);

namespace Drupal\ups_hazardous_materials;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;

/**
 * The UpsHazardousManager service to validate dangerous shipping items.
 */
class UpsHazardousManager {
  use LoggerChannelTrait;
  use StringTranslationTrait;

  protected const WEIGHT_UNIT_MAPPING = [
    'lb' => 'LBS',
    'kg' => 'KGS',
  ];

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Immutable config entity.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The logger channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs an UpsHazardousManager object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   */
  public function __construct(ClientInterface $http_client, ConfigFactoryInterface $configFactory) {
    $this->httpClient = $http_client;
    $this->config = $configFactory->get('ups_hazardous_materials.settings');
    $this->logger = $this->getLogger('ups_hazardous_materials');
  }

  /**
   * Extracts the hazardous items from the order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order to be analyzed.
   *
   * @return array
   *   A list of hazardous items.
   */
  protected function getHazardousOrderItems(OrderInterface $order): array {
    $hazardous_products = [];
    if (method_exists($order, 'getItems')) {
      foreach ($order->getItems() as $order_item) {
        $product_variation = $order_item->getPurchasedEntity();
        if ($product_variation->get('field_product_hazardous')->getValue()[0]['value'] !== '0') {
          // We add the order item since we need the quantity too.
          $hazardous_products[] = $order_item;
        }
      }
    }
    return $hazardous_products;
  }

  /**
   * Do validate an entire order for hazardous shipping.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order to validate.
   * @param array $configuration
   *   The specific configurations for this shipment.
   *
   * @return bool
   *   TRUE if order is validated.
   */
  public function validateOrder(OrderInterface $order, array $configuration): bool {
    $hazardous_products = $this->getHazardousOrderItems($order);
    if (empty($hazardous_products)) {
      return TRUE;
    }
    $store_address = $order->getStore()->getAddress()->getValue();
    $shipment = $order->get('shipments');
    $shipping_profile = [];
    if (method_exists($shipment, 'referencedEntities')) {
      $shipment_entities = $shipment->referencedEntities();
      $shipment = reset($shipment_entities);
      $shipping_profile = empty($shipment) ? [] : $shipment->getShippingProfile()->get('address')->getValue()[0];
    }
    return $this->doValidateOrder($store_address, $shipping_profile, $configuration, $hazardous_products, $shipment);
  }

  /**
   * Do validate an entire order for hazardous shipping.
   *
   * @param array $store_address
   *   The shipment "from" address.
   * @param array $shipping_address
   *   The shipment "to" address.
   * @param array $configuration
   *   The specific configurations for this shipment.
   * @param array $hazardous_items
   *   A list of purchasable items to check.
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment associated to the order.
   *
   * @return bool
   *   TRUE if order is validated.
   */
  private function doValidateOrder(array $store_address, array $shipping_address, array $configuration, array $hazardous_items, ShipmentInterface $shipment): bool {
    if ($shipment->getShippingMethod()->getPlugin()->getPluginDefinition()['id'] != 'ups') {
      $this->logger->error((string) $this->t("Dangerous goods must be shipped with UPS. Please contact the administrator."));
      return FALSE;
    }
    foreach ($hazardous_items as $hazardous_item) {
      if (!$this->validateItem($store_address, $shipping_address, $configuration, $hazardous_item, $shipment)) {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Validate a single purchased item for hazardous shipping.
   *
   * @param array $store_address
   *   The shipment "from" address.
   * @param array $shipping_address
   *   The shipment "to" address.
   * @param array $configuration
   *   The specific configurations for this shipment.
   * @param \Drupal\commerce_order\Entity\OrderItemInterface $hazardous_item
   *   A single purchasable item to check.
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment associated to the order.
   */
  private function validateItem(array $store_address, array $shipping_address, array $configuration, OrderItemInterface $hazardous_item, ShipmentInterface $shipment): bool {
    $data = self::getAcceptanceAuditPreCheckPayload($store_address, $shipping_address, $configuration, $hazardous_item, $shipment);

    try {
      $response = $this->executeAcceptanceAuditPreCheck($data, $shipment);
    }
    catch (ClientException | GuzzleException $e) {
      $this->logger->error($e->getMessage());
      return FALSE;
    }

    $status_code = $response->getStatusCode();
    $body = (string) $response->getBody();
    if ($status_code >= 200 && $status_code < 300) {
      return $this->parseAcceptanceAuditPreCheck($body);
    }
    else {
      $this->logger->error($body);
    }

    return FALSE;
  }

  /**
   * Get the request payload.
   *
   * @param array $store_address
   *   The shipment "from" address.
   * @param array $shipping_address
   *   The shipment "to" address.
   * @param array $configuration
   *   The specific configurations for this shipment.
   * @param \Drupal\commerce_order\Entity\OrderItemInterface $hazardous_item
   *   A single purchasable item to check.
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment associated to the order.
   *
   * @return string
   *   The JSON encoded payload.
   */
  private static function getAcceptanceAuditPreCheckPayload(array $store_address, array $shipping_address, array $configuration, OrderItemInterface $hazardous_item, ShipmentInterface $shipment): string {
    return JSON::encode([
      'AcceptanceAuditPreCheckRequest' => [
        'OriginRecordTransactionTimestamp' => self::getTransactionTimestamp(),
        'Shipment' => self::getShipmentPayload($store_address, $shipping_address, $configuration, $hazardous_item, $shipment),
      ],
    ]);
  }

  /**
   * Get the request timestamp in the desired format.
   *
   * @return string
   *   The formatted timestamp.
   */
  private static function getTransactionTimestamp(): string {
    return date('Y-m-d\TH:i:s') . '.000';
  }

  /**
   * Get "Shipment" payload data structure.
   *
   * @param array $store_address
   *   The "from" address.
   * @param array $shipping_address
   *   The "to" address.
   * @param array $configuration
   *   The pane configuration.
   * @param \Drupal\commerce_order\Entity\OrderItemInterface $hazardous_item
   *   A single purchasable item to check.
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The UPS shipment.
   *
   * @return array
   *   "Shipment" payload data structure.
   */
  private static function getShipmentPayload(array $store_address, array $shipping_address, array $configuration, OrderItemInterface $hazardous_item, ShipmentInterface $shipment): array {
    return [
      'ShipperNumber' => $configuration['shipper_number'],
      'ShipFromAddress' => self::getAddressPayload($store_address),
      'ShipToAddress' => self::getAddressPayload($shipping_address),
      'Service' => self::getServicePayload($shipment),
      'RegulationSet' => $configuration['regulation_set'],
      'Package' => self::getPackagePayload($configuration, $hazardous_item, $shipment),
    ];
  }

  /**
   * Get an address payload.
   *
   * @param array $address
   *   The address data.
   *
   * @return array
   *   The mapped address data.
   */
  private static function getAddressPayload(array $address): array {
    return [
      'AddressLine' => $address['address_line1'] . ' ' . $address['address_line2'],
      'City' => $address['locality'],
      'StateProvinceCode' => $address['administrative_area'],
      'PostalCode' => $address['postal_code'],
      'CountryCode' => $address['country_code'],
    ];
  }

  /**
   * Get the shipment service code.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment associated to the order.
   *
   * @return array
   *   The shipment code.
   */
  private static function getServicePayload(ShipmentInterface $shipment): array {
    return ['Code' => $shipment->getShippingService()];
  }

  /**
   * Get the "package" data structure.
   *
   * @param array $configuration
   *   The pane configuration.
   * @param \Drupal\commerce_order\Entity\OrderItemInterface $hazardous_item
   *   A single purchasable item to check.
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment associated to the order.
   *
   * @return array
   *   The package data structure
   */
  private static function getPackagePayload(array $configuration, OrderItemInterface $hazardous_item, ShipmentInterface $shipment): array {
    return [
      'PackageIdentifier' => '1',
      'PackageWeight' => self::getPackageWeight($shipment),
      'TransportationMode' => $configuration['transportation_mode'],
      'EmergencyPhone' => $configuration['emergency_phone'],
      'EmergencyContact' => $configuration['emergency_contact'],
      'ChemicalRecord' => self::getChemicalRecords($hazardous_item),
    ];
  }

  /**
   * Get the "package weight" data structure.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment associated to the order.
   *
   * @return array
   *   The package weight data structure
   */
  private static function getPackageWeight(ShipmentInterface $shipment): array {
    $weight = $shipment->getWeight();
    return [
      'Weight' => $weight->getNumber(),
      'UnitOfMeasurement' => ['Code' => self::WEIGHT_UNIT_MAPPING[$weight->getUnit()]],
    ];
  }

  /**
   * Get the "chemical record" data structure.
   *
   * @param \Drupal\commerce_order\Entity\OrderItemInterface $hazardous_item
   *   A single purchasable item to check.
   *
   * @return array
   *   The chemical record data structure
   */
  private static function getChemicalRecords(OrderItemInterface $hazardous_item): array {
    $value = [];
    $product_variation = $hazardous_item->getPurchasedEntity();
    $referenced_chemicals = $product_variation->get('field_chemical_records');
    $chemical_records = method_exists($referenced_chemicals, 'referencedEntities') ? $referenced_chemicals->referencedEntities() : [];
    $i = 0;
    foreach ($chemical_records as $chemical_record) {
      $i += 1;
      $value[] = [
        'ChemicalRecordIdentifier' => "$i",
        'ReportableQuantity' => $chemical_record->get('field_reportable_quantity')->value ? 'RQ' : '',
        'ClassDivisionNumber' => $chemical_record->get('field_class_division_number')->value,
        'SubRiskClass' => $chemical_record->get('field_sub_risk_class')->value,
        'IDNumber' => $chemical_record->label(),
        'PackagingGroupType' => $chemical_record->get('field_packaging_group_type')->value,
        'Quantity' => (float) $chemical_record->get('field_quantity')->value * $hazardous_item->getQuantity(),
        'UOM' => $chemical_record->get('field_unit_of_measurement')->value,
        'PackagingInstructionCode' => $chemical_record->get('field_packaging_instruction_code')->value,
        'ProperShippingName' => $chemical_record->get('field_proper_shipping_name')->value,
        'TechnicalName' => $chemical_record->get('field_technical_name')->value,
        'AdditionalDescription' => $chemical_record->get('field_additionaldescription')->value,
        'PackagingType' => $chemical_record->get('field_packaging_type')->value,
        'HazardLabelRequired' => $chemical_record->get('field_hazard_label_required')->value,
        'PackagingTypeQuantity' => $chemical_record->get('field_packaging_type_quantity')->value,
        'CommodityRegulatedLevelCode' => $chemical_record->get('field_commodity_regulated_level')->value,
      ];
    }
    return count($value) == 1 ? reset($value) : $value;
  }

  /**
   * Call the remote UPS API.
   *
   * @param string $data
   *   The JSON Payload.
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment associated to the order.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The API Response.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  private function executeAcceptanceAuditPreCheck(string $data, ShipmentInterface $shipment): ResponseInterface {
    $api_information = $shipment->getShippingMethod()->getPlugin()->getConfiguration()['api_information'];
    return $this->httpClient->request(
      'POST',
      $this->config->get('acceptance_audit_precheck_url'),
      [
        'headers' => [
          'Content-Type' => 'application/json',
          'transId' => $shipment->id(),
          'transactionSrc' => $api_information['user_id'],
          'AccessLicenseNumber' => $api_information['access_key'],
          'Username' => $api_information['user_id'],
          'Password' => $api_information['password'],
        ],
        'body' => $data,
      ]
    );
  }

  /**
   * Extracts data from the API response.
   *
   * @param string $response
   *   The API response.
   *
   * @return bool
   *   TRUE if the call was successful.
   */
  private function parseAcceptanceAuditPreCheck(string $response): bool {
    $body = JSON::decode($response);
    if (isset($body['AcceptanceAuditPreCheckResponse']['Response']['ResponseStatus']['Code'])) {
      return $body['AcceptanceAuditPreCheckResponse']['Response']['ResponseStatus']['Code'] === '1';
    }
    elseif (isset($body['response']['errors'])) {
      foreach ($body['response']['errors'] as $error) {
        $this->logger->error($error['code'] . ': ' . $error['message']);
      }
    }
    return FALSE;
  }

}

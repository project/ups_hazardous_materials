<?php

declare(strict_types=1);

namespace Drupal\ups_hazardous_materials\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Provides the Order summary pane.
 *
 * @CommerceCheckoutPane(
 *   id = "ups_hazardous",
 *   label = @Translation("UPS Hazardous"),
 *   default_step = "order_information",
 *   wrapper_element = "container",
 * )
 */
class UpsHazardous extends CheckoutPaneBase implements CheckoutPaneInterface {

  /**
   * The UPS Hazardous manager.
   *
   * @var \Drupal\ups_hazardous_materials\UpsHazardousManager
   */
  protected $upsHazardousManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow = NULL): UpsHazardous {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition, $checkout_flow);
    $instance->upsHazardousManager = $container->get('ups_hazardous_materials.ups_hazardous_manager');
    return $instance;
  }

  /**
   * Get the default configuration values mapped by key.
   *
   * @return string[]
   *   The key => default value array.
   */
  private static function getDefaultConfigurationValues(): array {
    return [
      'shipper_number' => '',
      'regulation_set' => '49CFR',
      'transportation_mode' => 'GND',
      'emergency_phone' => '',
      'emergency_contact' => '',
    ];
  }

  /**
   * Get the configuration labels mapped by key.
   *
   * @return string[]
   *   The key => label array.
   */
  private static function getDefaultConfigurationLabels(): array {
    return [
      'shipper_number' => 'Shipper Number',
      'regulation_set' => 'Regulation Set',
      'transportation_mode' => 'Transportation Mode',
      'emergency_phone' => 'Emergency Phone',
      'emergency_contact' => 'Emergency Contact',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return self::getDefaultConfigurationValues() + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationSummary(): string {
    $summary = '';
    foreach (self::getDefaultConfigurationLabels() as $config_name => $label) {
      $summary .= $label . ': ' . $this->configuration[$config_name] . '<br/>';
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    foreach (self::getDefaultConfigurationLabels() as $config_name => $label) {
      $form[$config_name] = [
        '#type' => 'textfield',
        '#title' => $label,
        '#default_value' => $this->configuration[$config_name],
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      foreach (array_keys(self::getDefaultConfigurationValues()) as $machine_name) {
        $this->configuration[$machine_name] = $values[$machine_name];
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function isVisible(): bool {
    if (method_exists($this->order, 'getItems')) {
      foreach ($this->order->getItems() as $order_item) {
        $product_variation = $order_item->getPurchasedEntity();
        if (!empty($product_variation->get('field_product_hazardous')->getValue()[0]['value'])) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form): array {
    $pane_form['hazardous_alert'] = [
      '#type' => 'container',
      'content' => ['#markup' => $this->t('This order contains Hazardous Materials and will be validated before proceeding.')],
      '#attributes' => [
        'class' => [
          'information',
          'information--warning',
        ],
      ],
    ];
    return $pane_form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitPaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    if (!$this->upsHazardousManager->validateOrder($this->order, $this->configuration)) {
      $this->messenger()->addError($this->t("The hazardous item shipment has not been validated. Please contact the administrator."));
      // @todo understand why form_state->setRedirect is not working
      $redirect = new RedirectResponse(Url::fromRoute(
        'commerce_checkout.form',
        [
          'commerce_order' => $this->order->id(),
          'step' => $complete_form['#step_id'],
        ]
      )->toString());
      $redirect->send();
    }
  }

}

<?php

declare(strict_types=1);

namespace Drupal\ups_hazardous_materials\Plugin\Commerce\Condition;

use Drupal\commerce\Plugin\Commerce\Condition\ConditionBase;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_shipping\Entity\ShippingMethodInterface;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the order has field value condition.
 *
 * @CommerceCondition(
 *   id = "condition_plugins_commerce_product_has_shipping_method",
 *   label = @Translation("Order contains product variations with this shipping"),
 *   display_label = @Translation("Order contains product variations with this shipping"),
 *   category = @Translation("Products"),
 *   entity_type = "commerce_order",
 * )
 */
class ProductHasShippingMethod extends ConditionBase implements ContainerFactoryPluginInterface {

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityBundleInfo;

  /**
   * Creates a new OrderHasBaseFieldValue instance.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityFieldManager $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_bundle_info
   *   The entity bundle info service.
   */
  final public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityFieldManager $entity_field_manager, EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $entity_bundle_info) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityBundleInfo = $entity_bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): ProductHasShippingMethod {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'bundle' => '',
      'base_field' => '',
      'shipping_id' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    // Load all the bundle types for the commerce_product_variation.
    $bundle_info = $this->entityBundleInfo->getBundleInfo('commerce_product_variation');
    $options = array_map(function ($item) {
      return $item['label'];
    }, $bundle_info);

    $values = $form_state->getValues();
    $bundle = isset($values['conditions']) && isset($values['conditions']['form']['products']) ? $values['conditions']['form']['products']['condition_plugins_commerce_product_has_shipping_method']['configuration']['form']['bundle'] : $this->configuration['bundle'];

    $form['bundle'] = [
      '#type' => 'select',
      '#title' => $this->t('Bundle'),
      '#options' => $options,
      '#empty_option' => $this->t('- Select -'),
      '#default_value' => $bundle,
      '#ajax' => [
        'callback' => [$this, 'myAjaxCallback'],
        'disable-refocus' => FALSE,
        'event' => 'change',
        'wrapper' => 'base-field-container',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Loading fields...'),
        ],
      ],
    ];

    if (!empty($bundle)) {
      $options = [];
      // Load all the field for the selected bundle.
      foreach ($this->entityFieldManager->getFieldDefinitions('commerce_product_variation', $bundle) as $field_name => $field_definition) {
        $options[$field_name] = $field_definition->getLabel();
      }

      $base_field = isset($values['conditions']) && isset($values['conditions']['form']['products']) ? $values['conditions']['form']['products']['condition_plugins_commerce_product_has_shipping_method']['configuration']['form']['base_field'] : $this->configuration['base_field'];

      $form['base_field'] = [
        '#type' => 'select',
        '#title' => $this->t('Field'),
        '#options' => $options,
        '#empty_option' => $this->t('- Select -'),
        '#default_value' => $base_field,
        '#prefix' => '<div id="base-field-container">',
        '#suffix' => '</div>',
      ];
    }
    else {
      $form['base_field'] = [
        '#type' => 'container',
        '#id' => 'base-field-container',
      ];
    }
    // Place the hidden shipping_id since it must be kept after submit.
    $form['shipping_id'] = [
      '#type' => 'hidden',
      '#value' => $this->configuration['shipping_id'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function myAjaxCallback(array $form, FormStateInterface $form_state): array {
    // Rebuild the form and send only the child select.
    return $this->buildConfigurationForm($form, $form_state)['base_field'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    parent::submitConfigurationForm($form, $form_state);

    $values = $form_state->getValues();
    $this->configuration['bundle'] = $values['conditions']['form']['products']['condition_plugins_commerce_product_has_shipping_method']['configuration']['form']['bundle'];
    $this->configuration['base_field'] = $values['conditions']['form']['products']['condition_plugins_commerce_product_has_shipping_method']['configuration']['form']['base_field'];
    $this->configuration['shipping_id'] = $values['conditions']['form']['products']['condition_plugins_commerce_product_has_shipping_method']['configuration']['form']['shipping_id'];
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate(EntityInterface $entity): bool {
    $this->assertEntity($entity);
    assert($entity instanceof OrderInterface);
    $configuration = $this->getConfiguration();
    // For every cart item, I check if it's a commerce_product_variation.
    foreach ($entity->getItems() as $order_item) {
      $product_variation = $order_item->getPurchasedEntity();
      // I check that the configured field exists for every variation.
      if ($product_variation->bundle() == $configuration['bundle'] && $product_variation->hasField($configuration['base_field'])) {
        $field_value = $product_variation->get($configuration['base_field']);
        if (method_exists($field_value, 'referencedEntities')) {
          $referenced_entities = $field_value->referencedEntities();
          $shipping_method = reset($referenced_entities);
          // If the field referenced entity is the same as the condition, pass.
          if ($shipping_method instanceof ShippingMethodInterface && $shipping_method->id() == $configuration['shipping_id']) {
            return TRUE;
          }
        }
      }
    }

    return FALSE;
  }

}
